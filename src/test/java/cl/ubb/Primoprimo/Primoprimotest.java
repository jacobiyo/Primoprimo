package cl.ubb.Primoprimo;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;

import org.junit.Test;

public class Primoprimotest {

	@Test
	public void ingresarceroretornarnulo() {
		/*arrange*/
		boolean resultado;
		Primoprimo p = new Primoprimo();
		/*act*/
		resultado=p.determinarprimo(0);
		/*assert*/
		assertThat(resultado,is(false));
	}

}
